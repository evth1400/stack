//
// Created by Eva on 2017-11-04.
//

#ifndef Stack_H
#define Stack_H

#include<memory>
#include "memstat.hpp"

using namespace std;

template<typename T>
class Node{
private:
    Node* prevNode;
    T value;

public:
    Node(Node* aPrev, T aVal) : prevNode(aPrev), value(aVal){};
    ~Node(){};
    T getValue(){return value;};
    Node* getPrevNode(){return prevNode;};
};

template<typename T>
class Stack{
private:
    Node<T>* top = nullptr;
    size_t stackSize = 0;

public:
    Stack(){};
    ~Stack();

    bool empty(){return stackSize == 0;};
    size_t size(){ return stackSize;};
    T pop();
    void push(T addVal);
};

template<typename T>
T Stack<T>::pop(){
    if (empty()) {
        throw std::range_error{"Can't pop empty stack"};
    }

    Node<T>* temp = top;
    top = temp->getPrevNode();

    --stackSize;

    T val = temp->getValue();
    delete temp;

    return val;
}

template<typename T>
void Stack<T>::push(T addVal){
    Node<T>* oldTop = top;

    top = new Node<T>(oldTop,addVal);
    ++stackSize;
}

template<typename T>
Stack<T>::~Stack() {
    while (stackSize > 0){
        pop();
    }
}

#endif



